<?php
/**
* Plugin Name:99minutos.com
* Plugin URI: http://99minutos.com
* Description:The fastest shipping for online shopping
* Version: 1.2.6
* Author: 99 minutos
* Author URI: http://99minutos.com
* License: GPL2
*/
if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// Derive the current path and load up WooCommerce
if( class_exists('Woocommerce') != true )
	require_once( WP_PLUGIN_DIR.'/woocommerce/woocommerce.php' );

/**
 * Check if WooCommerce is active
 **/
if ( in_array( 'woocommerce/woocommerce.php', apply_filters( 'active_plugins', get_option( 'active_plugins' ) ) ) )
{

    $url = get_site_url();
    $content = array
    (
        "Shop" => $url,
    );

    $chi = curl_init('http://api.99minutos.com/woo/install.php');
     curl_setopt_array($chi, array(
         CURLOPT_POST => TRUE,
				 CURLOPT_SSLVERSION	=> 1,
         CURLOPT_RETURNTRANSFER => TRUE,
         CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
         CURLOPT_POSTFIELDS => json_encode($content)
     ));
		$response = curl_exec($chi);

		curl_close ($chi);
   	function woo_shipping99min_sameday_init(){
        if( !class_exists( 'WC_Woo_Shipping99min_sameday' ) ){
        	class WC_Woo_Shipping99min_sameday extends WC_Shipping_Method{
        		public function __construct() {

					//Register plugin text domain for translations files
					load_plugin_textdomain( 'wooshipping99minsameday', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

					$this->id = '99_sameday';
					$this->method_title = __( '99 Minutos Mismo Día', 'wooshipping99minsameday' );
					$this->shipping_sameday_option = 'woocommerce_shipping_samedays';
					$this->init();
				}

				public function init() {
					$this->init_form_fields();
					$this->init_settings();

					// Define user set variables
					$this->title 		  = $this->get_option( 'title' );
                    $this->availability   = $this->get_option( 'availability' );
					$this->countries 	  = $this->get_option( 'countries' );
					$this->type 		  = $this->get_option( 'type' );
					$this->cost 		  = $this->get_option( 'cost' );
					$this->tax_per_kg    = $this->get_option( 'tax_per_kg' );
					$this->increase = $this->get_option( 'increase' );

					// Load Increase Rates
					$this->get_shipping_samedays();

				}
     			/*
				*	Run during the activation of the plugin
				*/
				function activate() {

					if ( ! current_user_can( 'activate_plugins' ) )
						return;
					$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
					check_admin_referer( "activate-plugin_{$plugin}" );

					# Uncomment the following line to see the function in action
					# exit( var_dump( $_GET ) );
				}

				public static function deactivate()
				{
					if ( ! current_user_can( 'activate_plugins' ) )
						return;
					$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
					check_admin_referer( "deactivate-plugin_{$plugin}" );

					# Uncomment the following line to see the function in action
					# exit( var_dump( $_GET ) );
				}

				public function init_form_fields() {

					$this->form_fields = array(
					'enabled' => array(
									'title' 		=> __( 'Enable/Disable', 'woocommerce' ),
									'type' 			=> 'checkbox',
									'label' 		=> __( 'Enable this shipping method', 'woocommerce' ),
									'default' 		=> 'yes'
								),
					'title'  => array(
									'title' 		=> __( 'Shipping', 'woocommerce' ),
									'type' 			=> 'text',
                                    'label' 		=>__( '99minutos.com.Programado mismo día (L-V de 9 a 16 hrs)','woocommerce' ),
                                   'default'		=> 'yes'
                    ),
					);
				}

				function calculate_shipping( $package = array() ) {
					global $woocommerce;
					$total_weight = 0;
					$final_increase = 0;


					$this->rates = array();

                    $total_weight = $this->get_package_weight( $package );
                    $postal_code = $package['destination']['postcode'];
                    $country = $package['destination']['country'];
                    $contents_cost = $package['contents_cost'];
                    $url = get_site_url();

                    $json = array (
                        "Shop" =>$url,
                        "total_weight" => $total_weight ,
                        "postal_code" => $postal_code ,
                        "country" => $country,
                        "contents_cost" => $contents_cost,
                        "Delivery_type" => 'Schedule',
                        );
                    $ch = curl_init('http://api.99minutos.com/woo/shippingrate.php');
                    curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
										CURLOPT_SSLVERSION	=> 1,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
                    CURLOPT_POSTFIELDS => json_encode($json)
                    ));

                    $response = curl_exec($ch);
                    $responseData = json_decode($response, TRUE);
                    if ($responseData['Status'] == 'OK')
                    {
                        $rate = array(
							'id' 	=> $this->id,
							'label' => $responseData['Rates']['Schedule']['Title'],
							'cost' 	=> $responseData['Rates']['Schedule']['Cost'],
							);
                    }
					if ( isset( $rate ) )
						$this->add_rate( $rate );
				}

				function get_package_weight( $package = array() ){
					$total_weight = 0;
					if ( sizeof( $package['contents'] ) > 0 ) {
						foreach ( $package['contents'] as $item_id => $values ) {
							if ( $values['data']->has_weight() ) {
								$products_weight = $values['data']->get_weight() * $values['quantity'];
								$total_weight = $total_weight + $products_weight;
							}
						}
					}
					return $total_weight;
				}

				function get_shipping_samedays() {
					$this->shipping_samedays = array_filter( (array) get_option( $this->shipping_sameday_option ) );
				}

				function is_available( $package ) {
					global $woocommerce;

					if ($this->enabled=="no") return false;

						if ($this->availability=='including') :

							if (is_array($this->countries)) :
								if ( ! in_array( $package['destination']['country'], $this->countries) ) return false;
							endif;

						else :

							if (is_array($this->countries)) :
								if ( in_array( $package['destination']['country'], $this->countries) ) return false;
							endif;

						endif;

					return apply_filters( 'woocommerce_shipping_' . $this->id . '_is_available', true );
				}
	  }
	 }
    }

   	function woo_shipping99min_express_init(){
    	if( !class_exists( 'WC_Woo_Shipping99min_express' ) ){
			class WC_Woo_Shipping99min_express extends WC_Shipping_Method{
				public function __construct() {

					//Register plugin text domain for translations files
					load_plugin_textdomain( 'wooshipping99minexpress', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

					$this->id = '99_Minutos';
					$this->method_title = __( '99 Minutos Express', 'wooshipping99minexpress' );
					$this->shipping_expres_option = 'woocommerce_shipping_express';
					$this->init();
				}
				public function init() {

					$this->init_form_fields();
					$this->init_settings();

					// Define user set variables
					$this->title 		  = $this->get_option( 'title' );
                    $this->availability   = $this->get_option( 'availability' );
					$this->countries 	  = $this->get_option( 'countries' );
					$this->type 		  = $this->get_option( 'type' );
					$this->cost 		  = $this->get_option( 'cost' );
					$this->tax_per_kg    = $this->get_option( 'tax_per_kg' );
					$this->increase = $this->get_option( 'increase' );

					// Load Increase Rates
					$this->get_shipping_express();
                }
				function activate() {

					if ( ! current_user_can( 'activate_plugins' ) )
						return;
					$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
					check_admin_referer( "activate-plugin_{$plugin}" );

					# Uncomment the following line to see the function in action
					# exit( var_dump( $_GET ) );
				}
				public static function deactivate(){
					if ( ! current_user_can( 'activate_plugins' ) )
						return;
					$plugin = isset( $_REQUEST['plugin'] ) ? $_REQUEST['plugin'] : '';
					check_admin_referer( "deactivate-plugin_{$plugin}" );

					# Uncomment the following line to see the function in action
					# exit( var_dump( $_GET ) );
				}
				public function init_form_fields() {

					$this->form_fields = array(
					'enabled' => array(
									'title' 		=> __( 'Enable/Disable', 'woocommerce' ),
									'type' 			=> 'checkbox',
									'label' 		=> __( 'Enable this shipping method', 'woocommerce' ),
									'default' 		=> 'yes'
								),
					'title'  => array(
									'title' 		=> __( 'Shipping_Express', 'woocommerce' ),
									'type' 			=> 'text',
                                    'label' 		=>__( '99minutos.com.Envio en 99 Minutos (L-V de 9 a 16 hrs)','woocommerce' ),
                                   'default'		=> 'yes'
                    ),
					);
				}
                function calculate_shipping( $package = array() ) {
					global $woocommerce;
					$total_weight = 0;
					$final_increase = 0;

					$this->rates = array();

                    $total_weight = $this->get_package_weight( $package );
                    $postal_code = $package['destination']['postcode'];
                    $country = $package['destination']['country'];
                    $contents_cost = $package['contents_cost'];
                    $url = get_site_url();

                    $json = array (
                        "Shop" =>$url,
                        "total_weight" => $total_weight ,
                        "postal_code" => $postal_code ,
                        "country" => $country,
                        "contents_cost" => $contents_cost,
                        "Delivery_type" => 'Express',
                        );

                    $ch = curl_init('http://api.99minutos.com/woo/shippingrate.php');
                    curl_setopt_array($ch, array(
                    CURLOPT_POST => TRUE,
										CURLOPT_SSLVERSION	=> 1,
                    CURLOPT_RETURNTRANSFER => TRUE,
                    CURLOPT_HTTPHEADER => array('Content-Type: application/json'),
                    CURLOPT_POSTFIELDS => json_encode($json)
                    ));

                    $response = curl_exec($ch);
                    $responseData = json_decode($response, TRUE);
                    if ($responseData['Status'] == 'OK')
                    {
                        $rate = array(
							'id' 	=> $this->id,
							'label' => $responseData['Rates']['Express']['Title'],
							'cost' 	=> $responseData['Rates']['Express']['Cost'],
							);
                    }
					if ( isset( $rate ) )
						$this->add_rate( $rate );
				}

				function get_package_weight( $package = array() ){
					$total_weight = 0;
					if ( sizeof( $package['contents'] ) > 0 ) {
						foreach ( $package['contents'] as $item_id => $values ) {
							if ( $values['data']->has_weight() ) {
								$products_weight = $values['data']->get_weight() * $values['quantity'];
								$total_weight = $total_weight + $products_weight;
							}
						}
					}

					return $total_weight;
				}

				function get_shipping_express() {
					$this->shipping_express = array_filter( (array) get_option( $this->shipping_expres_option ) );
				}

				function is_available( $package ) {
					global $woocommerce;

					if ($this->enabled=="no") return false;

						if ($this->availability=='including') :

							if (is_array($this->countries)) :
								if ( ! in_array( $package['destination']['country'], $this->countries) ) return false;
							endif;

						else :

							if (is_array($this->countries)) :
								if ( in_array( $package['destination']['country'], $this->countries) ) return false;
							endif;

						endif;

					return apply_filters( 'woocommerce_shipping_' . $this->id . '_is_available', true );
				}
		}
    }
}

   	add_action( 'woocommerce_shipping_init', 'woo_shipping99min_sameday_init' );
   	add_action( 'woocommerce_shipping_init', 'woo_shipping99min_express_init' );

    function add_99_sameday_method( $methods ) {

		$methods[] = 'WC_Woo_Shipping99min_sameday';
        return $methods;
	}

    function add_99_Minutos_method( $methods ) {

		$methods[] = 'WC_Woo_Shipping99min_express'; return $methods;
	}

    	add_filter( 'woocommerce_shipping_methods', 'add_99_sameday_method');
    	add_filter( 'woocommerce_shipping_methods', 'add_99_Minutos_method' );
}

?>
