# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* The 99minute plugin has been stored
* Version: 1.2.6
* (https://Alejandro10@bitbucket.org/Alejandro10/woo_shipping99minutos.git)

### How do I get set up? ###

* We go to the tab where it says plugins and we give click to add plugin.
* We give the button to add file and we look for our plugin where we have it stored and we      upload it.
* After installing it will not activate, we will create a webhook.
* The webhook will carry the following:
Nombre:          99minutos_shipping 
Estado:            Activo 
Tema:               Pedido actualizado 
Url de entrega: http://api.99minutos.com/woo/shipping.php
* Once the webhook is created, we activate the plugin of 99minutos.
* We communicate with the systems area of ​​99minutes so that they finish the process and you can use the plugin without any problem.
* Contact to communicate to the systems area is to the mail hola@99minutos.com


### Who do I talk to? ###

* 99minutos
* 99minutos.com or hola@99minutos.com